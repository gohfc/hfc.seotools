﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ExCSS;
using HFC.SeoTools.Logging;
using HtmlAgilityPack;

namespace HFC.SeoTools.LazyLoading
{
    internal class LazyLoadingFilterInsert
    {
        private static readonly Regex ExtensionRegex = new Regex("\\.(ico|gif|bmp|xbm|jpg|jpeg|webp|png|apng)$",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static readonly ILog log = LogProvider.For<LazyLoadingFilterInsert>();


        private static void AddLazyLoadClass(HtmlNode node)
        {
            var attributeValue = node.GetAttributeValue("class", "");
            var classes = attributeValue.Split(' ').ToList();
            if (!classes.Contains("lazyload"))
                node.SetAttributeValue("class",
                    string.IsNullOrEmpty(attributeValue) ? "lazyload" : string.Format("{0} lazyload", attributeValue));
        }

        private static bool InjectLazyLoadForBackgroundImages(HtmlDocument htmlDocument,
            List<HtmlNode> negateLazyLoadElements)
        {
            var changed = false;
            //look for nodes withOUT attribute data-bg, and with a style attribute that contains the word 'background'
            var htmlNodeCollection =
                htmlDocument.DocumentNode.SelectNodes("//*[not(@data-bg) and contains(@style,'background')]");
            if (htmlNodeCollection == null || htmlNodeCollection.Count == 0)
            {
                if (log.IsDebugEnabled())
                    log.Debug("No nodes with style attribute were found.");

                return false;
            }

            // check wrapper elements and make sure they're not 'no-lazy-load' elements
            var list2 = htmlNodeCollection.Where(i => !i.AncestorsAndSelf().Any(negateLazyLoadElements.Contains))
                .ToList();
            if (log.IsDebugEnabled())
                log.DebugFormat(
                    "Found {0} nodes with style attribute and no data-bg attribute, and {1} of those nodes don't have a parent with nolazyload class",
                    htmlNodeCollection.ToList().Count(), list2.Count);
            var parser = new Parser();
            foreach (var node in list2)
            {
                var style = node.GetAttributeValue("style", null);
                if (!string.IsNullOrWhiteSpace(style))
                {
                    var lazyLoadElementInfo = GetBackgroundLazyLoadElementInfo(style, parser);
                    if (lazyLoadElementInfo.Changed)
                    {
                        node.SetAttributeValue("data-bg", lazyLoadElementInfo.ImageUrl);
                        if (string.IsNullOrWhiteSpace(lazyLoadElementInfo.UpdatedStyle))
                            node.Attributes.Remove("style");
                        else
                            node.SetAttributeValue("style", lazyLoadElementInfo.UpdatedStyle);

                        AddLazyLoadClass(node);
                        changed = true;
                    }
                }
            }

            return changed;
        }

        private static BackgroundLazyLoadElementInfo GetBackgroundLazyLoadElementInfo(string style, Parser parser)
        {
            var elementInfo = new BackgroundLazyLoadElementInfo();


            try
            {
                var stylesheet = parser.Parse(string.Format(".d{{{0}}}", style));

                if (stylesheet.StyleRules.Any())
                {
                    var styleRule = stylesheet.StyleRules[0];

                    var declarations = styleRule.Declarations
                        .Where(i => i.Name == "background-image" || i.Name == "background").ToList();
                    foreach (var declaration in declarations)
                    {
                        var removeDeclaration = false;
                        var singlePrimitive = declaration.Term as PrimitiveTerm;
                        if (singlePrimitive != null && singlePrimitive.PrimitiveType == UnitType.Uri)
                        {
                            elementInfo.ImageUrl = singlePrimitive.Value.ToString();
                            removeDeclaration = true;
                        }
                        else
                        {
                            var termList = declaration.Term as TermList;
                            var primitiveFromList = termList == null
                                ? null
                                : termList.OfType<PrimitiveTerm>()
                                    .FirstOrDefault(i => i.PrimitiveType == UnitType.Uri);
                            if (primitiveFromList != null)
                            {
                                elementInfo.ImageUrl = primitiveFromList.Value.ToString();
                                //make a new list that excludes primitives of Uri type
                                var newTermList = new TermList();
                                foreach (var term in termList)
                                {
                                    var primitiveTerm = term as PrimitiveTerm;

                                    if (primitiveTerm == null || primitiveTerm.PrimitiveType != UnitType.Uri)
                                        newTermList.AddTerm(term);
                                }

                                if (newTermList.Any())
                                    declaration.Term = newTermList;
                                else
                                    removeDeclaration = true;
                            }
                        }

                        if (removeDeclaration) styleRule.Declarations.Remove(declaration);
                    }

                    if (elementInfo.ImageUrl != null)
                    {
                        var newStyle = styleRule.Declarations.ToString();
                        elementInfo.UpdatedStyle = string.IsNullOrWhiteSpace(newStyle) ? null : newStyle;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("Issue with checking style on element", ex);
            }

            return elementInfo;
        }

        private static bool InjectLazyLoadForRegularImages(HtmlDocument htmlDocument,
            List<HtmlNode> negateLazyLoadElements)
        {
            var changed = false;
            var list1 = htmlDocument.DocumentNode.SelectNodes("//img");
            if (list1 == null || list1.Count == 0)
            {
                if (log.IsDebugEnabled()) log.Debug("No image nodes found");
                return false;
            }

            var list2 = list1.Where(i =>
                !i.AncestorsAndSelf().Any(negateLazyLoadElements.Contains)).ToList();
            if (log.IsDebugEnabled())
                log.DebugFormat(
                    "Found {0} image nodes, and {1} of those nodes don't have parents with nolazyload attribute or class",
                    list1.Count, list2.Count);

            foreach (var node in list2)
            {
                var src = node.GetAttributeValue("src", null);
                if (!string.IsNullOrEmpty(src) && IsImagePath(src))
                {
                    node.SetAttributeValue("data-src", src);
                    node.Attributes.Remove("src");
                    AddLazyLoadClass(node);
                    changed = true;
                }
            }

            return changed;
        }

        private static bool IsImagePath(string src)
        {
            return ExtensionRegex.IsMatch(src);
        }

        public static void Filter(HtmlDocument document, SeoToolsFilterOptions options)
        {
            var lazyLoadingOptions = options.LazyLoadingOptions;
            var nodes = document.DocumentNode.SelectNodes("//*")?.ToList() ?? new List<HtmlNode>();
            //find all elements marked with attribute "nolazyload"
            var negateLazyLoadElements =
                nodes.Where(i =>
                    i.HasAnyClass(lazyLoadingOptions.ClassesForIgnore.Union(new[] {"nolazyload"})) ||
                    i.HasAnyAttribute(lazyLoadingOptions.AttributesForIgnore.Union(new[] {"nolazyload"}))).ToList();

            //see if any background images need to be lazy loaded and aren't marked up already
            var injectedBackground = InjectLazyLoadForBackgroundImages(document, negateLazyLoadElements);

            //see if any regular images need to be lazy loaded and aren't marked up already
            var injectedNormal = InjectLazyLoadForRegularImages(document, negateLazyLoadElements);

            if (injectedBackground || injectedNormal)
                if (lazyLoadingOptions.InjectLazySizes)
                {
                    var head = document.DocumentNode.SelectSingleNode("//head");
                    if (head == null)
                    {
                        var body = document.DocumentNode.SelectSingleNode("//body");
                        if (body != null)
                        {
                            head = body.OwnerDocument.CreateElement("head");
                            body.AppendChild(head);
                        }
                    }

                    if (head != null)
                    {
                        var script = document.CreateElement("script");
                        script.SetAttributeValue("src", lazyLoadingOptions.LazySizesUrl);
                        if (injectedBackground)
                        {
                            var lazyBeforeUnveilScript = document.CreateElement("script");
                            lazyBeforeUnveilScript.AppendChild(document.CreateTextNode(Resource1.LazyBeforeUnveilText));
                            head.AppendChild(lazyBeforeUnveilScript);
                        }
                    }
                }
        }
    }
}