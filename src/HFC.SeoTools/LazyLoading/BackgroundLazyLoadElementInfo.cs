﻿using System;

namespace HFC.SeoTools.LazyLoading
{
    internal class BackgroundLazyLoadElementInfo
    {
        public bool Changed => !String.IsNullOrWhiteSpace(ImageUrl);

        public string UpdatedStyle { get; set; }
        public string ImageUrl { get; set; }
    }
}