﻿using System.Collections.Generic;
using HFC.SeoTools.Configuration;

namespace HFC.SeoTools.LazyLoading
{
    public class LazyLoadingOptions : OptionsBase
    {
        /// <summary>
        ///     Aside from class 'nolazyload', ignore items that have any of these classes
        /// </summary>
        public List<string> ClassesForIgnore { get; } = new List<string>();

        /// <summary>
        ///     Aside from attribute 'nolazyload', ignore items that have any of these attributes
        /// </summary>
        public List<string> AttributesForIgnore { get; } = new List<string>();

        /// <summary>
        ///     Whether or not we should inject the LazySizes javascript
        /// </summary>
        public bool InjectLazySizes { get; set; }

        /// <summary>
        ///     Source url of the LazySizes javascript.  You can point this to a local URL as opposed to the CDN
        /// </summary>
        public string LazySizesUrl { get; set; } = "//cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.1/lazysizes.min.js";
    }
}