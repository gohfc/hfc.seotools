﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.TL.BIZ.Base
{
    public class MasterPage: System.Web.UI.MasterPage
    {
        public string DirName {
            get { 
                return HFC.TL.BIZ.Util.CookieManager.DirName;
            }
        
        }
    }
}
