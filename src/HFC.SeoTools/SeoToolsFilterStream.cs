﻿using System;
using System.IO;
using HFC.SeoTools.CacheBusting;
using HFC.SeoTools.DeferredJavascript;
using HFC.SeoTools.LazyLoading;
using HtmlAgilityPack;

namespace HFC.SeoTools
{
    public class SeoToolsFilterStream : Stream
    {
        private readonly MemoryStream _cacheStream = new MemoryStream();

        private readonly Stream _filter;
        private readonly SeoToolsFilterOptions _options;

        public SeoToolsFilterStream(Stream filter, SeoToolsFilterOptions options)
        {
            _filter = filter;
            _options = options;
        }

        public override bool CanRead => _cacheStream.CanRead;

        public override bool CanSeek => _cacheStream.CanSeek;

        public override bool CanWrite => _cacheStream.CanWrite;

        public override long Length => _cacheStream.Length;

        public override long Position
        {
            get => _cacheStream.Position;
            set => _cacheStream.Position = value;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _cacheStream.Write(buffer, 0, count);
        }

        private void UpdateHtml(MemoryStream inputStream, Stream outputStream)
        {
            var lazyHtmlDocument = new Lazy<HtmlDocument>(() =>
            {
                var htmlDocument = new HtmlDocument {OptionEmptyCollection = true};
                htmlDocument.Load(inputStream);
                return htmlDocument;
            });
            if (_options.DeferredJavascriptOptions.Enabled)
                DeferredJavascriptFilterInsert.Filter(lazyHtmlDocument.Value, _options);
            if (_options.CacheBustingOptions.Enabled)
                CacheBustingFilterInsert.Filter(lazyHtmlDocument.Value, _options);
            if (_options.LazyLoadingOptions.Enabled)
                LazyLoadingFilterInsert.Filter(lazyHtmlDocument.Value, _options);
            //if no processing took place, just copy to output stream
            if (lazyHtmlDocument.IsValueCreated)
                lazyHtmlDocument.Value.Save(outputStream);
            else
                inputStream.CopyTo(outputStream);
        }

        public override void Flush()
        {
            if (_cacheStream.Length > 0)
            {
                _cacheStream.Position = 0;
                UpdateHtml(_cacheStream, _filter);
                _cacheStream.SetLength(0);
            }

            _filter.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _cacheStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _cacheStream.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _cacheStream.Read(buffer, offset, count);
        }
    }
}