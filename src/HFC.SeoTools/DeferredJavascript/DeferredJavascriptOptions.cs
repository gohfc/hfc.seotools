﻿using HFC.SeoTools.Configuration;

namespace HFC.SeoTools.DeferredJavascript
{
    public class DeferredJavascriptOptions : OptionsBase
    {
        /// <summary>
        ///     If true, then inline scripts will be minified
        /// </summary>
        public bool MinifyInlineScript { get; set; } = true;

        /// <summary>
        ///     If true, then adjacent inline script nodes will be merged together
        /// </summary>
        public bool MergeAdjacentInlineScriptNodes { get; set; } = true;
    }
}