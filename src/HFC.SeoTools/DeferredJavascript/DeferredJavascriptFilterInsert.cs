﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using HFC.SeoTools.Logging;
using HtmlAgilityPack;
using Microsoft.Ajax.Utilities;

namespace HFC.SeoTools.DeferredJavascript
{
    public class DeferredJavascriptFilterInsert
    {
        private static readonly ILog log = LogProvider.For<DeferredJavascriptFilterInsert>();

        internal static string MinimizeAndBase64EncodeJavascript(string scriptInnerText, SeoToolsFilterOptions options)
        {
            if (options.DeferredJavascriptOptions.MinifyInlineScript)
            {
                var minifier = new Minifier();
                try
                {
                    scriptInnerText =
                        minifier.MinifyJavaScript(scriptInnerText, new CodeSettings {CollapseToLiteral = true});
                }
                catch (Exception ex)
                {
                    log.WarnException("Can't minify javascript", ex);
                }
            }

            var format = string.Format("data:text/javascript;base64,{0}",
                Convert.ToBase64String(Encoding.Default.GetBytes(scriptInnerText)));
            return format;
        }

        private static void MergeInlineScriptNodes(List<HtmlNode> inlineScriptNodes, HtmlDocument htmlDocument)
        {
            var attrs = new Dictionary<HtmlNode, string>();
            //make a dictionary of the attributes attached to each script element
            foreach (var node in inlineScriptNodes)
            {
                var typeAttribute = node.Attributes.AttributesWithName("type").FirstOrDefault();
                if (typeAttribute != null && typeAttribute.Value == "text/javascript")
                    node.Attributes.Remove(typeAttribute);
                var languageAttribute = node.Attributes.AttributesWithName("language").FirstOrDefault();
                if (languageAttribute != null) node.Attributes.Remove(languageAttribute);
                var attributeStringBuilder = new StringBuilder();
                foreach (var attribute in node.Attributes.OrderBy(i => i.Name))
                    attributeStringBuilder.AppendFormat("{0}={1};", HttpUtility.UrlEncode(attribute.Name),
                        HttpUtility.UrlEncode(attribute.Value));
                attrs[node] = attributeStringBuilder.ToString();
            }

            var skippedNodes = new List<HtmlNode>();

            var index = 0;
            while (index < inlineScriptNodes.Count)
            {
                var indexNode = inlineScriptNodes[index];
                var referenceNode = indexNode;
                while (true)
                {
                    //look for node's next sibling
                    var siblingNode = referenceNode.NextSibling;
                    var textNode = siblingNode as HtmlTextNode;

                    //match on text nodes
                    if (textNode != null)
                    {
                        if (string.IsNullOrWhiteSpace(textNode.Text))
                        {
                            referenceNode = textNode;
                            skippedNodes.Add(textNode);
                        }

                        continue;
                    }

                    //match on comment nodes
                    var commentNode = siblingNode as HtmlCommentNode;
                    if (commentNode != null)
                    {
                        referenceNode = commentNode;
                        skippedNodes.Add(commentNode);

                        continue;
                    }

                    //see if sibling is in our list, and maybe we can merge the javascript from both nodes
                    if (inlineScriptNodes.Contains(siblingNode))
                    {
                        //merge if attributes match
                        if (attrs[indexNode] == attrs[siblingNode])
                        {
                            var newScript = string.Format("\r\n{0}\r\n{1}\r\n",
                                indexNode.InnerText.TrimEnd().TrimStart(), siblingNode.InnerText.TrimEnd().TrimStart());
                            indexNode.ChildNodes.Clear();
                            indexNode.ChildNodes.Add(htmlDocument.CreateTextNode(newScript));
                            inlineScriptNodes.Remove(siblingNode);
                            siblingNode.ParentNode.RemoveChild(siblingNode);
                            referenceNode = indexNode;
                            if (skippedNodes.Any())
                                foreach (var skippedNode in skippedNodes)
                                    skippedNode.ParentNode.RemoveChild(skippedNode);

                            skippedNodes.Clear();
                        }
                        else
                        {
                            skippedNodes.Clear();

                            break;
                        }
                    }
                    else
                    {
                        skippedNodes.Clear();
                        break;
                    }
                }

                index++;
            }
        }

        public static void Filter(HtmlDocument document, SeoToolsFilterOptions options)
        {
            try
            {
                //find scripts with blank type attribute or where type attribute="text/javascript"
                var scripts = document.DocumentNode.SelectNodes("//script").Select((i, j) => new
                    {
                        type = i.GetAttributeValue("type", null),
                        script = i,
                        index = j,
                        IsInline = string.IsNullOrEmpty(i.GetAttributeValue("src", null))
                    }).Where(i => i.script.Attributes.All(ji => ji.Name != "nodefer") && (
                                      string.IsNullOrWhiteSpace(i.type) ||
                                      i.type.Equals("text/javascript", StringComparison.InvariantCultureIgnoreCase)))
                    .ToList();

                var externalItems = scripts.Where(i => !i.IsInline).ToList();


                foreach (var item in externalItems)
                    if (string.IsNullOrEmpty(item.script.GetAttributeValue("defer", null)))
                        item.script.SetAttributeValue("defer", "defer");

                var inlineScriptNodes =
                    scripts.Where(i => i.IsInline).OrderBy(i => i.index).Select(i => i.script).ToList();

                if (options.DeferredJavascriptOptions.MergeAdjacentInlineScriptNodes)
                {
                    MergeInlineScriptNodes(inlineScriptNodes, document);
                }

                MinimizeAndDeferInlineScriptNodes(inlineScriptNodes, options);
            }
            catch (Exception ex)
            {
                log.WarnException("Can't manipulate script block", ex);
            }
        }

        private static void MinimizeAndDeferInlineScriptNodes(List<HtmlNode> inlineScriptNodes, SeoToolsFilterOptions options)
        {
            foreach (var script in inlineScriptNodes)
            {
                var scriptInnerText = script.InnerText;
                var format = MinimizeAndBase64EncodeJavascript(scriptInnerText, options);
                script.SetAttributeValue("src",
                    format);
                script.ChildNodes.Clear();
                script.SetAttributeValue("defer", "defer");
            }
        }
    }
}