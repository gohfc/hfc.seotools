﻿using HFC.SeoTools.CacheBusting;
using HFC.SeoTools.DeferredJavascript;
using HFC.SeoTools.LazyLoading;

namespace HFC.SeoTools
{
    public class SeoToolsFilterOptions
    {
        public DeferredJavascriptOptions DeferredJavascriptOptions { get; } = new DeferredJavascriptOptions();
        public CacheBustingOptions CacheBustingOptions { get; } = new CacheBustingOptions();
        public LazyLoadingOptions LazyLoadingOptions { get; } = new LazyLoadingOptions();
    }
}