﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace HFC.SeoTools
{
    internal static class HtmlNodeExtensions
    {
        public static bool HasAnyClass(this HtmlNode htmlNode, IEnumerable<string> classNames)
        {
            return htmlNode.GetAttributeValue("class", string.Empty)
                .Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries)
                .Join(classNames, i => i, i => i, (i, j) => i).Any();
        }

        public static bool HasAnyAttribute(this HtmlNode htmlNode, IEnumerable<string> attributeName)
        {
            return attributeName.Any(i => htmlNode.GetAttributeValue(i, null) != null);
        }
    }
}