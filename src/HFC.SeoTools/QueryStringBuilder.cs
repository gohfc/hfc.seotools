﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace HFC.SeoTools
{
    internal class QueryStringBuilder : Dictionary<string, string>
    {
        public QueryStringBuilder()
        {
        }

        public QueryStringBuilder(NameValueCollection nameValueCollection)
        {
            foreach (string key in nameValueCollection.Keys) this[key] = nameValueCollection[key];
        }

        public QueryStringBuilder(IDictionary<string, string> input) : base(input)
        {
        }

        public override string ToString()
        {
            return string.Join("&",
                this.Where(i => !string.IsNullOrEmpty(i.Value)).OrderBy(i => i.Key).Select(i =>
                    string.Format("{0}={1}", HttpUtility.UrlEncode(i.Key), HttpUtility.UrlEncode(i.Value))));
        }
    }
}