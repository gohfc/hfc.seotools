﻿using System;
using HFC.SeoTools.Configuration;

namespace HFC.SeoTools.CacheBusting
{
    public class CacheBustingOptions : OptionsBase
    {
        public string QueryStringProperty { get; set; } = "hfc-r";

        public string CacheBustingFilterPath { get; set; }
        public ICacheBustingFilterSource FilterSource { get; set; } = new DefaultCacheBustingFilterSource();
        public TimeSpan KeyChangeInterval { get; set; } = TimeSpan.FromMinutes(15);
    }
}