﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Caching;
using HFC.SeoTools.Logging;
using Newtonsoft.Json;

namespace HFC.SeoTools.CacheBusting
{
    public class JsonCacheBustingFilterSource : ICacheBustingFilterSource
    {
        private const string filterCacheKey = "6fc9c81e-9022-4907-8dbb-5195fbc61268";
        private static readonly ILog log = LogProvider.For<JsonCacheBustingFilterSource>();
        public string Filename { get; set; }

        public List<CacheBustingFilter> GetFilters()
        {
            var list = MemoryCache.Default.Get(filterCacheKey) as List<CacheBustingFilter>;
            if (list == null)
                try
                {
                    list = JsonConvert.DeserializeObject<List<CacheBustingFilter>>(File.ReadAllText(Filename));
                    MemoryCache.Default.Set(filterCacheKey, list,
                        new CacheItemPolicy
                        {
                            AbsoluteExpiration = DateTimeOffset.MaxValue,
                            ChangeMonitors = {new HostFileChangeMonitor(new List<string> {Filename})}
                        });
                }
                catch (Exception ex)
                {
                    log.ErrorException(
                        "Can't get cache busting filter info from file '{0}'. Will retry in ten minutes.", ex,
                        Filename);
                    list = new List<CacheBustingFilter>();
                    MemoryCache.Default.Set(filterCacheKey, list,
                        new CacheItemPolicy
                        {
                            AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(10)
                        });
                }

            return list;
        }
    }
}