﻿using System.Collections.Generic;

namespace HFC.SeoTools.CacheBusting
{
    public interface ICacheBustingFilterSource
    {
        List<CacheBustingFilter> GetFilters();
    }
}