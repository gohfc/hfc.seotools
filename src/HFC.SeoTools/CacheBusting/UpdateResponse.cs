﻿namespace HFC.SeoTools.CacheBusting
{
    internal class UpdateResponse
    {
        public static readonly UpdateResponse NotChanged = new UpdateResponse(null, false);

        public UpdateResponse(string url, bool changed)
        {
            Url = url;
            Changed = changed;
        }

        public string Url { get; }
        public bool Changed { get; }
    }
}