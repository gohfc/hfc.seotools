﻿using System.Collections.Generic;

namespace HFC.SeoTools.CacheBusting
{
    public class DefaultCacheBustingFilterSource : ICacheBustingFilterSource
    {
        public List<CacheBustingFilter> GetFilters()
        {
            return new List<CacheBustingFilter>
            {
                new CacheBustingFilter
                {
                    XPath = "//link[@rel='stylesheet']",
                    AttributeNameForImage = "href"
                },
                new CacheBustingFilter
                {
                    XPath = "//source[@srcset]",
                    AttributeNameForImage = "srcset"
                },
                new CacheBustingFilter
                {
                    XPath = "//script[@src]",
                    AttributeNameForImage = "src"
                },
                new CacheBustingFilter
                {
                    XPath = "//img[@src]",
                    AttributeNameForImage = "src"
                }
            };
        }
    }
}