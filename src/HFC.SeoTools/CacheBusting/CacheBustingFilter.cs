﻿using Newtonsoft.Json;

namespace HFC.SeoTools.CacheBusting
{
    public class CacheBustingFilter
    {
        [JsonProperty("xpath")] public string XPath { get; set; }

        [JsonProperty("attributeNameForImage")]
        public string AttributeNameForImage { get; set; }
    }
}