﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Caching;
using System.Text;
using HFC.SeoTools.Logging;
using HtmlAgilityPack;

namespace HFC.SeoTools.CacheBusting
{
    internal class CacheBustingFilterInsert
    {
        private const string CacheKey = "60e58469-3757-4b5b-8e8d-3c3e066cdf03";

        private const string AppliedAttribute = "data-cache-busting-applied";
        private static readonly object Mutex = new object();
        private static readonly ILog log = LogProvider.For<CacheBustingFilterInsert>();

        private static readonly Uri FakeUri = new Uri("http://fakedomain.com");


        public static void Filter(HtmlDocument document, SeoToolsFilterOptions seoFilterOptions)
        {
            try
            {
                var list = seoFilterOptions.CacheBustingOptions.FilterSource.GetFilters();

                var lazyLoadList = new List<CacheBustingFilter>();

                //add two more filters if lazy loading is turned on
                if (seoFilterOptions.LazyLoadingOptions.Enabled)
                    lazyLoadList.AddRange(new[]
                    {
                        new CacheBustingFilter
                        {
                            XPath = "//img[@data-src]",
                            AttributeNameForImage = "data-src"
                        },

                        new CacheBustingFilter
                        {
                            XPath = "//*[@data-bg]",
                            AttributeNameForImage = "data-bg"
                        }
                    });


                foreach (var item in list.Union(lazyLoadList))
                foreach (var node in document.DocumentNode.SelectNodes(item.XPath).ToList())
                    UpdateNode(node, item.AttributeNameForImage, seoFilterOptions);
            }
            catch (Exception ex)
            {
                log.WarnException("Can't cache-bust document", ex);
            }
        }

        private static void UpdateNode(HtmlNode node, string attributeName,
            SeoToolsFilterOptions options)
        {
            //make sure we don't process the same node twice
            if (node.GetAttributeValue(AppliedAttribute, null) == null)
            {
                var attributeValue = node.GetAttributeValue(attributeName, null);
                if (!string.IsNullOrWhiteSpace(attributeValue))
                {
                    //see if we should update the url
                    var updateResponse = TryUpdateUrl(attributeValue, options);
                    if (updateResponse.Changed)
                    {
                        //change the attribute value to the updated url
                        node.SetAttributeValue(attributeName, updateResponse.Url);
                        //mark the node so we don't process it again
                        node.SetAttributeValue(AppliedAttribute, "true");
                    }
                }
            }
        }

        private static string GetRandomString(SeoToolsFilterOptions options)
        {
            lock (Mutex)
            {
                var cachedItem = MemoryCache.Default.Get(CacheKey) as string;
                if (cachedItem == null)
                {
                    var random = new Random();
                    var sb = new StringBuilder();
                    for (var i = 0; i < 6; i++) sb.Append((char) (random.Next(0, 1) * 32 + random.Next(65, 90)));
                    cachedItem = sb.ToString();
                    MemoryCache.Default.Set(CacheKey, cachedItem,
                        DateTimeOffset.Now.Add(options.CacheBustingOptions.KeyChangeInterval));
                }

                return cachedItem;
            }
        }

        private static UpdateResponse TryUpdateUrl(string url, SeoToolsFilterOptions options)
        {
            var cacheBustingOptions = options.CacheBustingOptions;
            try
            {
                if (string.IsNullOrWhiteSpace(url)) return UpdateResponse.NotChanged;
                var randomValue = GetRandomString(options);
                Uri uri;
                if (url.StartsWith("//"))
                    //for protocol-relative urls such as //yahoo.com/somefile.html
                    return UpdateResponse.NotChanged;

                if (Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out uri))
                {
                    if (uri.IsAbsoluteUri) return new UpdateResponse(url, false);
                    ;

                    var uriBuilder = new UriBuilder(new Uri(FakeUri, url));
                    var qsb = new QueryStringBuilder(uriBuilder.Uri.ParseQueryString());

                    if (!qsb.ContainsKey(cacheBustingOptions.QueryStringProperty))
                        qsb[cacheBustingOptions.QueryStringProperty] = randomValue;
                    uriBuilder.Query = qsb.ToString();
                    return new UpdateResponse($"{uriBuilder.Uri.PathAndQuery}{uriBuilder.Uri.Fragment}", true);
                }
            }
            catch (Exception ex)
            {
                log.WarnException(string.Format("Can't cache-bust url {0}", url), ex);
            }

            return UpdateResponse.NotChanged;
        }
    }
}