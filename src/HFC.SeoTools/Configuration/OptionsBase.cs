﻿namespace HFC.SeoTools.Configuration
{
    public abstract class OptionsBase
    {
        public bool Enabled { get; set; } = true;
    }
}